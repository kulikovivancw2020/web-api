﻿namespace BussinesLogicLayer.Interfaces
{
    public interface IService<T> where T: class
    {
        public void Create(T obj);
        public T Get(int id);
        public void Update(int id, string newValue);
        public void Delete(int id);
    }
}

﻿using DataAcsessLayer.Interfaces;
using DataAcsessLayer.Models;
using DataAcsessLayer.Repositoreas;

namespace DataAcsessLayer.Unit_of_work
{
    public class Unit_of_work
    {
        private readonly IRepository<Users> _userRepository;
        
        public Unit_of_work(UserRepository repository)
        {
            _userRepository = repository;
        }

        public IRepository<Users> UserRepository
        {
            get
            {
                return _userRepository;
            }
        }
    }
}

﻿using DataAcsessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAcsessLayer.EntiryContext
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Users> User { get; set; }
    }
}

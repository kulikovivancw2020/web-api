﻿using System.ComponentModel.DataAnnotations;

namespace DataAcsessLayer.Models
{
    public class Users
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [StringLength(maximumLength:25,MinimumLength = 3)]
        public string UserName { get; set; }

        [Required]
        [StringLength(maximumLength: 25,MinimumLength = 3,ErrorMessage = "Not Correct login")]
        public string Login { get; set; }
        
        [Required]
        [StringLength(maximumLength: 25, MinimumLength = 3, ErrorMessage = "Not Correct password")]
        public string Password { get; set; }

        public string AboutYou { get; set; }

        [Phone]
        public string ContactNumber { get; set; }

        [EmailAddress]
        public string ContactEmail { get; set; }
    }
}

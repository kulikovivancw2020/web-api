﻿using BussinesLogicLayer.DTO;
using BussinesLogicLayer.Interfaces;
using DataAcsessLayer.Models;
using DataAcsessLayer.Unit_of_work;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System;

namespace BussinesLogicLayer.Services
{
    public class UserService : IService<DTOUsers>
    {
        private readonly Unit_of_work _unitOfWork;

        private readonly ILogger<UserService> _logger;

        public UserService(Unit_of_work unitOfWork, ILogger<UserService> logger)
        {
            _unitOfWork = unitOfWork;

            _logger = logger;
        }

        public void Create(DTOUsers obj)
        {
            _logger.LogInformation("Start mapping from DTOUsers to Users");

            try
            {
                var config = new MapperConfiguration(mc => mc.CreateMap<DTOUsers, Users>());
                var mapper = new Mapper(config);
                var userModel = mapper.Map<DTOUsers, Users>(obj);

                _unitOfWork.UserRepository.Create(userModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                _unitOfWork.UserRepository.Delete(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public DTOUsers Get(int id)
        {
            Users users = _unitOfWork.UserRepository.Get(id);

            _logger.LogInformation("Start mapping from Users to DTOUsers");

            var config = new MapperConfiguration(mc => mc.CreateMap<Users, DTOUsers>());
            var mapper = new Mapper(config);
            var dtoUserModel = mapper.Map<Users, DTOUsers>(users);

            return dtoUserModel;
        }

        public void Update(int id, string newValue)
        {
            try
            {
                _unitOfWork.UserRepository.Update(id, newValue);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}

﻿using BussinesLogicLayer.DTO;
using BussinesLogicLayer.Interfaces;
using BussinesLogicLayer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IService<DTOUsers> _service;

        private readonly ILogger<UserController> _logger;

        public UserController(UserService service, ILogger<UserController> logger)
        {
            _service = service;

            _logger = logger;
        }

        [HttpGet]
        [Route("/create_new_user")]
        public ActionResult CreateNewUser(string userName, string login, string password, string contactNumber, string aboutYou, string contactEmail)
        {
            DTOUsers dTOUsers = new()
            {
                UserName = userName,
                Login = login,
                Password = password,
                ContactNumber = contactNumber,
                AboutYou = aboutYou,
                ContactEmail = contactEmail
            };

            _logger.LogInformation("Created new user ({0})", dTOUsers.UserName);

            try
            {
                if (ModelState.IsValid)
                {
                    _service.Create(dTOUsers);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }

            return Ok();
        }

        [HttpDelete]
        [Route("/delete_user")]
        public ActionResult DeleteUser(int id)
        {
            _logger.LogInformation("use delete method: id = {0}", id);

            _service.Delete(id);

            _logger.LogInformation("id: {0} - use deleted", id);

            return Ok();
        }

        [HttpGet]
        [Route("/update_password")]
        public ActionResult UpdateUser(int id, string password)
        {
            _logger.LogInformation("id: {0} want update password", id);

            _service.Update(id, password);

            _logger.LogInformation("id: {0} - password is updated", id);

            return Ok();
        }

        [HttpPost]
        [Route("/get_user")]
        public ActionResult<DTOUsers> GetUser(int id)
        {
            DTOUsers users = _service.Get(id);

            _logger.LogInformation("User: {0}, id: {1} use soft", users.UserName, users.Id);

            return users;
        }
    }
}

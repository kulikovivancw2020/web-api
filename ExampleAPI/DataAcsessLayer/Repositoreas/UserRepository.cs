﻿using DataAcsessLayer.EntiryContext;
using DataAcsessLayer.Interfaces;
using DataAcsessLayer.Models;
using Microsoft.Extensions.Logging;
using System;

namespace DataAcsessLayer.Repositoreas
{
    public class UserRepository : IRepository<Users>
    {
        private readonly DataBaseContext _context;

        private readonly ILogger<UserRepository> _logger;

        public UserRepository(DataBaseContext context, ILogger<UserRepository> logger)
        {
            _context = context;

            _logger = logger;
        }

        public void Create(Users obj)
        {
            _logger.LogInformation("Creating user {0}",obj.UserName);

            try
            {
                _context.Add(obj);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                _logger.LogInformation("Search user[id: {0}] in database", id);

                var user = _context.User.Find(id);

                _context.User.Remove(user);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public Users Get(int id)
        {
            _logger.LogInformation("Search user[id: {0}] in database", id);

            var user = _context.User.Find(id);

            _context.SaveChanges();

            return user;
        }

        public void Update(int id, string newValue)
        {
            try
            {
                _logger.LogInformation("Search user[id: {0}] in database", id);

                var user = _context.User.Find(id);

                user.Password = newValue;

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
